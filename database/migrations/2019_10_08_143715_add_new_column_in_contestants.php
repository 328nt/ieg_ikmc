<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnInContestants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contestants', function (Blueprint $table) {
            $table->string('shirt_size')->nullable()->default('M')->after('logistic');
            $table->string('book')->nullable()->after('logistic');
            $table->string('phone_teacher')->nullable()->after('logistic');
            $table->string('username')->nullable()->after('logistic');
            $table->string('sbd')->nullable()->after('logistic');
            $table->string('payment')->nullable()->after('school_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contestants', function (Blueprint $table) {
            //
        });
    }
}
