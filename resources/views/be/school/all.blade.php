@extends('be.layouts.index')
@section('title')
list
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Tất cả các trường</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>District</th>
                        {{-- <th>school_code</th>
                        <th>District</th> --}}
                        {{-- <th>District</th>
                        <th>Province</th> --}}
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>District</th>
                        {{-- <th>school_code</th>
                        <th>District</th> --}}
                        {{-- <th>District</th>
                        <th>Province</th> --}}
                    </tr>
                </tfoot>
                {{-- <tbody>
                    @foreach ($schools as $cont)
                    <tr>
                        <td>{{$cont->id}}</td>
                        <td>{{$cont->name}}</td>
                        <td>{{$cont->districts->name}}</td>
                        <td>{{$cont->districts->provinces->name}}</td>
                    </tr>
                    @endforeach
                </tbody> --}}


            </table>

        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            "ajax": 'admin/schools/json',
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "district_id" },
                // { "data": null,
                // render: function(data, type, row){
                //     return data.district_id;
                // }
                //  },
            ]
        });
    })
</script>
@endsection