@extends('be.layouts.index')
@section('title')
Danh sách trường đã thanh toán
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Trường đã đăng ký</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>District</th>
                        <th>Province</th>
                        <th>Payment</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>District</th>
                        <th>Province</th>
                        <th>Payment</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($school as $cont)
                    @if (count($cont->contestants) > 0)

                    <tr>
                        <td>{{$cont->id}}</td>
                        <td>{{$cont->name}}</td>
                        <td>{{$cont->districts->name}}</td>
                        <td>{{$cont->districts->provinces->name}}</td>
                        <td>
                            
                            @if ($cont->payment == 1)
                            <a href="admin/schools/schooledit/{{$cont->id}}"><img width="50px" src="img/paid.png" alt=""></a>
                            @else
                            <a href="admin/schools/schooledit/{{$cont->id}}"><img width="60px" src="img/unpaid.jpg" alt=""></a>
                            @endif
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>


            </table>

        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            //"ajax": './assets/demo/data/table_data.json',
            /*"columns": [
                { "data": "name" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/
        });
    })
</script>
@endsection