@extends('be.layouts.index')
@section('title')
Sửa thanh toán trường
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Thông tin trường</div>
        </div>
        <div class="ibox-body">
        <form action="admin/schools/schooledit/{{$school->id}}" method="post" enctype="multipart/form-data" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tên:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="name" type="text" value="{{$school->name}}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Quận/huyện:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="email" type="text" value="{{$school->districts->name}}" disabled >
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tỉnh/thành:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="email" type="text" value="{{$school->districts->provinces->name}}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tình trạng thanh toán:</label>
                    <div class="col-sm-10">
                        <select name="payment" id="">
                            <option value="0" 
                            @if ($school->payment == 0)
                                {{"selected"}}
                            @endif
                            >chưa thanh toán</option>
                            <option value="1"
                            @if ($school->payment == 1)
                                {{"selected"}}
                            @endif>đã thanh toán</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection