@extends('be.layouts.index')
@section('title')
list
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Trường đã đăng ký</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>District</th>
                        <th>button</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>District</th>
                        <th>button</th>
                    </tr>
                </tfoot>
                {{-- <tbody>
                    @foreach ($schools as $cont)
                    @if (count($cont->contestants) > 0)

                    <tr>
                        <td>{{$cont->id}}</td>
                        <td>{{$cont->name}}</td>
                        <td>

                            @if ($cont->payment == 1)
                            <img width="50px" src="img/paid.png" alt="">
                            @else
                            <img width="60px" src="img/unpaid.jpg" alt="">
                            @endif
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody> --}}


            </table>

        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            "ajax": 'admin/schools/json',
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "district_id" },
            ]
        });
    })

    $(document).ready(function() {
    var table = $('#example-table').DataTable( {
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   {
            leftColumns: 2
        }
    } );
} );
</script>
@endsection