@extends('be.layouts.index')
@section('title')
Thêm trường
@endsection
@section('content')

@include('msg')
<div class="col-md-8">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Thêm trường</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="{{route('school_add')}}" method="post" enctype="multipart/form-data" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Tỉnh/Thành phố</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="province" id="province">
                            <option value="" selected disabled hidden>--Chọn tinh/thành phố--</option>
                            @foreach ($provinces as $prov)
                            <option value="{{ $prov->id }}">{{ $prov->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Quận/Huyện</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="district" id="district">
                            <option value="" selected disabled hidden>--Chọn quận/huyện--</option>
                            {{-- @foreach ($districts as $dist)
                            <option value="{{ $dist->id }}">{{ $dist->provinces->name }} {{ $dist->name }}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Trường</label>
                    <div class="col-sm-8">
                        <input class="form-control" name="school" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">ID</label>
                    <div class="col-sm-8">
                        <input class="form-control" name="id" type="text">
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')

<script>
    $(document).ready(function() {
        $("#province").change(function(){
            var prov_id = $(this).val();
            $.get("ajax/district/"+prov_id, function(data){
             $("#district").html(data);
            });
        });
    }); 
    $(document).ready(function() {
        $("#district").change(function(){
            var district_id = $(this).val();
            $.get("ajax/school/"+district_id, function(data){
             $("#school").html(data);
            });
        });
    }); 
</script>
@endsection