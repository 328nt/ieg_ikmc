@extends('be.layouts.index')
@section('title')
list
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">{{$school->name}}</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>Fullname</th>
                        <th>DoB</th>
                        <th>Gender</th>
                        <th>Class</th>
                        <th>Grade</th>
                        <th>Level</th>
                        <th>School</th>
                        <th>District</th>
                        <th>Province</th>
                        <th>Parent name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th>Logistic</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>Fullname</th>
                        <th>DoB</th>
                        <th>Gender</th>
                        <th>Class</th>
                        <th>Grade</th>
                        <th>Level</th>
                        <th>School</th>
                        <th>District</th>
                        <th>Province</th>
                        <th>Parent name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th>Logistic</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($school->contestants as $cont)
                    <tr>
                        <td>{{$cont->id}}</td>
                        <td>{{$cont->name}}</td>
                        <td>{{$cont->fullname}}</td>
                        <td>{{$cont->dob}}</td>
                        <td>{{$cont->gender}}</td>
                        <td>{{$cont->class}}</td>
                        <td>{{$cont->grade}}</td>
                        <td>{{$cont->level}}</td>
                        <td>{{$cont->school->name}}</td>
                        <td>{{$cont->school->districts->name}}</td>
                        <td>{{$cont->school->districts->provinces->name}}</td>
                        <td>{{$cont->parentname}}</td>
                        <td>{{$cont->email}}</td>
                        <td>{{$cont->phone}}</td>
                        <td>{{$cont->address}}</td>
                        <td>{{$cont->logistic}}</td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a
                                href="admin/contestants/edit/{{$cont->id}}">Edit</a></td>
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/contestants/delete/{{$cont->id}}"> Delete</a></td>

                    </tr>
                    @endforeach
                </tbody>


            </table>
            
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            //"ajax": './assets/demo/data/table_data.json',
            /*"columns": [
                { "data": "name" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/
        });
    })
</script>
@endsection