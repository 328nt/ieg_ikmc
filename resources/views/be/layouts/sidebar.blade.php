<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="assets/img/admin.png" width="45px" />
            </div>
            <div class="admin-info">
                <div class="font-strong">{{Auth::User()->name}}</div><small></small>
            </div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="admin/dashboard"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">FEATURES</li>


            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Danh sách thí sinh</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="javascript:;">Danh sách tổng</a>
                    </li>
                    <li>
                        <a href="javascript:;">Thí sinh đã thanh toán</a>
                    </li>
                    <li>
                        <a href="javascript:;">Thí sinh chưa thanh toán</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="nav-label">Thí sinh đăng ký</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            <li>
                                <a href="javascript:;">Thí sinh lẻ</a>
                            </li>
                            <li>
                                <a href="javascript:;">Thí sinh đăng ký theo trường</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/schools*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Danh sách trường</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/schools/all">Tất cả các trường</a>
                    </li>
                    <li>
                        <a href="admin/schools/add">Thêm trường</a>
                    </li>
                    <li>
                        <a href="admin/schools/schoolreg">Trường đã Đăng ký</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="nav-label">Danh sách trường</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            <li>
                                <a href="admin/schools/schoolreg">Trường đã đăng ký</a>
                            </li>
                            <li>
                                <a href="admin/schools/schoolpaid">Trường đã thanh toán</a>
                            </li>
                            <li>
                                <a href="admin/schools/schoolunpaid">Trường chưa thanh toán</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;">Trường chưa thanh toán</a>
                    </li>
                </ul>
            </li>

            <li class="{{ request()->is('admin/contestants*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bookmark"></i>
                    <span class="nav-label">Danh sách thí sinh</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/contestants/list"><i class="sidebar-item-icon fa fa-list-alt"></i>Danh sách tổng</a>
                    </li>
                    <li>
                        <a href="admin/contestants/sort_list"><i class="sidebar-item-icon fa fa-list"></i>Danh sách rút gọn</a>
                    </li>
                    {{-- <li>
                        <a href="javascript:;">
                            <span class="nav-label">Thí sinh theo trường</span><i
                                class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            @foreach ($schools as $school)
                            @if (count($school->contestants) > 0)
                            <li>
                                <a href="admin/schools/list/{{$school->id}}">{{$school->name}}</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </li> --}}
                    <li>
                        <a href="admin/contestants/add"><i class="sidebar-item-icon fa fa-plus-circle"></i>Thêm thí sinh</a>
                    </li>
                    <li>
                        <a href="admin/contestants/send_mail"><i class="sidebar-item-icon fa fa-envelope"></i>Gửi mail thanh toán</a>
                    </li>
                </ul>
            </li>
            <li class="heading">ADMINS</li>


            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bookmark"></i>
                    <span class="nav-label">Quản trị viên</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/users/list">Danh sách</a>
                    </li>
                    <li>
                        <a href="admin/users/add">Thêm</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Mailbox</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="mailbox.html">Inbox</a>
                    </li>
                    <li>
                        <a href="mail_view.html">Mail view</a>
                    </li>
                    <li>
                        <a href="mail_compose.html">Compose mail</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="calendar.html"><i class="sidebar-item-icon fa fa-calendar"></i>
                    <span class="nav-label">Calendar</span>
                </a>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-file-text"></i>
                    <span class="nav-label">Pages</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="invoice.html">Invoice</a>
                    </li>
                    <li>
                        <a href="profile.html">Profile</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                    <li>
                        <a href="register.html">Register</a>
                    </li>
                    <li>
                        <a href="lockscreen.html">Lockscreen</a>
                    </li>
                    <li>
                        <a href="forgot_password.html">Forgot password</a>
                    </li>
                    <li>
                        <a href="error_404.html">404 error</a>
                    </li>
                    <li>
                        <a href="error_500.html">500 error</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Menu Levels</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="javascript:;">Level 2</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="nav-label">Level 2</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            <li>
                                <a href="javascript:;">Level 3</a>
                            </li>
                            <li>
                                <a href="javascript:;">Level 3</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>