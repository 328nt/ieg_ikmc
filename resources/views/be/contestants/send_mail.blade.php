@extends('be.layouts.index')
@section('title')
send mail
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Danh sách bài dự thi</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>sbd</th>
                        <th>Fullname</th>
                        <th>DoB</th>
                        <th>Class</th>
                        <th>Parent name</th>
                        <th>Email</th>
                        <th>Amount</th>
                        <th>payment</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($contestants as $cont)
                    @if ($cont->payment == 1 && $cont->send_mail == 0)
                    <tr>
                        <td>{{$cont->id}}</td>
                        <td>{{$cont->sbd}}</td>
                        <td>{{$cont->fullname}}</td>
                        <td>{{$cont->dob}}</td>
                        <td>{{$cont->class}}</td>
                        <td>{{$cont->dad_name}} <br>
                            {{$cont->mom_name}} <br>
                            {{$cont->parentname}}
                        </td>
                        <td>{{$cont->email}}</td>
                        <td>{{$cont->amount}}</td>
                        <td>
                            @if ($cont->payment == 1)
                            paid <i class="fa fa-check"></i> <br>
                            @if ($cont->send_mail == 0)
                            <p hidden>z</p>
                            <form action="{{url('testmail')}}" method="post">
                                {{ csrf_field() }}
                                <input type="email" class="form-control" name="email" value="{{$cont->email}}">
                                <input type="text" class="form-control" name="id" hidden value="{{$cont->id}}">
                                <input type="submit" name="submit" value="Sendmail" class="submit-btn">
                            </form>
                            @endif
                            @endif
                        </td>
                    </tr>
                    @endif


                    {{-- @if ($cont->payment == 0 && $cont->send_mail == 1)
                        
                    <tr>
                        <td>{{$cont->id}}</td>
                        <td>{{$cont->sbd}}</td>
                        <td>{{$cont->fullname}}</td>
                        <td>{{$cont->dob}}</td>
                        <td>{{$cont->class}}</td>
                        <td>{{$cont->dad_name}} <br>
                            {{$cont->mom_name}} <br>
                            {{$cont->parentname}}
                        </td>
                        <td>{{$cont->email}}</td>
                        <td>{{$cont->amount}}</td>
                        <td>
                            <form action="{{url('fixmybad')}}" method="post">
                                {{ csrf_field() }}
                                <input type="email" class="form-control" name="email" value="{{$cont->email}}">
                                <input type="text" class="form-control" name="id" hidden value="{{$cont->id}}">
                                <input type="submit" name="submit" value="Sendmail" class="submit-btn">
                            </form>
                        </td>

                    </tr>
                    @endif --}}

                    @endforeach
                </tbody>


            </table>
        </div>
        <hr>

    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
        "order": [[ 8, "desc" ]]
            // pageLength: 10,
            // "ajax": 'admin/contestants/json',
            // "columns": [
            //     { "data": "id" },
            //     { "data": "name" },
            //     { "data": "fullname" },
            //     { "data": "dob" },
            //     { "data": "gender" },
            //     { "data": "class" },
            //     { "data": "grade" },
            //     { "data": "level" },
            //     { "data": "school_id" },
            //     { "data": "level" },
            //     { "data": "level" },
            //     { "data": "parentname" },
            //     { "data": "email" },
            //     { "data": "phone" },
            //     { "data": "address" },
            //     { "data": "logistic" },
            //     { "data": "payment",
            //     render: function(data, type, row){
            //     if(parseInt(data) == 0){
            //         return 'chưa thanh toán';
            //     }
            //     else if(parseInt(data) == 1){
            //         return 'đã thanh toán';
            //     }
            //      }
            //     },
            //     { "data": "phone" },
            //     { "data": "address" },
            // ]
        });
    })
</script>
@endsection