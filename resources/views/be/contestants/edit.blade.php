@extends('be.layouts.index')
@section('title')
edit contestants
@endsection
@section('content')

@include('msg')
<div class="col-md-12">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Sửa thí sinh</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="{{route('contestants_edit', $cont->id)}}" method="post" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tên</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="name" type="text" value="{{$cont->name}}" placeholder="Tên">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tên đầy đủ</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="fullname" type="text" value="{{$cont->fullname}}" placeholder="Tên đầy đủ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Ngày sinh</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="dob" type="text" value="{{$cont->dob}}" placeholder="Tên đầy đủ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Giới tính</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="gender" id="">
                                    <option value="0">Nam</option>
                                    <option value="1">Nữ</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tỉnh/Thành phố</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="province" id="province">
                                    <option value="" selected disabled hidden>--Select--</option>
                                    @foreach ($provinces as $prov)
                                    <option
                                    @if ($cont->school->districts->provinces->id == $prov->id)
                                        {{"selected"}}
                                    @endif
                                value="{{$prov->id}}">{{$prov->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Quận/Huyện</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="district" id="district">
                                    <option value="" selected disabled hidden>--Select--</option>
                                    @foreach ($districts as $dist)
                                    <option
                                    @if ($cont->school->districts->id == $dist->id)
                                        {{"selected"}}
                                    @endif
                                    value="{{$dist->id}}">{{$dist->name}}</option>
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Trường</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="school_id" id="school">
                                    <option value="" selected disabled hidden>--Select--</option>
                                    @foreach ($schools as $scl)
                                    <option @if ($cont->school->id == $scl->id)
                                        {{"selected"}}
                                    @endif value="{{$scl->id}}">{{$scl->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Khối</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="grade" type="text" value="{{$cont->grade}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Lớp</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="class" type="text" value="{{$cont->class}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Cấp độ</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="level" type="text" value="{{$cont->level}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tên Phụ huynh</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="parentname" type="text" value="{{$cont->parentname}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="email" type="text" value="{{$cont->email}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">sđt</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="phone" type="text" value="{{$cont->phone}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Địa chỉ</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="address" type="text" value="{{$cont->address}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Địa chỉ nhận sách</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="logistic" type="text" value="{{$cont->logistic}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Thanh toán</label>
                            <div class="col-sm-8">
                                <select name="payment" id="">
                                    <option @if ($cont->payment == 0)
                                        {{"selected"}}
                                    @endif value="0">chưa thanh toán</option>
                                    <option @if ($cont->payment == 1)
                                        {{"selected"}}
                                    @endif value="1">đã thanh toán</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-8 ml-sm-auto">
                                <button class="btn btn-info" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')

<script>
    $(document).ready(function(){
            $("#changepassword").change(function(){
                if($(this).is(":checked"))
                {
                    $(".password").removeAttr('disabled');
                }
                else
                {
                    $(".password").attr('disabled','');
                }
            })
        })
</script>

<script src="assets/js/scripts/form-plugins.js" type="text/javascript"></script>
<script type="text/javascript">
    $("#form-sample-1").validate({
        rules: {
            name: {
                minlength: 2,
                required: !0
            },
            email: {
                required: !0,
                email: !0
            },
            url: {
                required: !0,
                url: !0
            },
            number: {
                required: !0,
                number: !0
            },
            min: {
                required: !0,
                minlength: 3
            },
            max: {
                required: !0,
                maxlength: 4
            },
            password: {
                required: !0
            },
            password_confirmation: {
                required: !0,
                equalTo: "#password"
            }
        },
        errorClass: "help-block error",
        highlight: function(e) {
            $(e).closest(".form-group.row").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group.row").removeClass("has-error")
        },
    });
</script>


<script>
    $(document).ready(function() {
            $("#province").change(function(){
                var prov_id = $(this).val();
                $.get("ajax/district/"+prov_id, function(data){
                 $("#district").html(data);
                });
            });
        }); 
</script>
<script>
    $(document).ready(function() {
            $("#district").change(function(){
                var district_id = $(this).val();
                $.get("ajax/school/"+district_id, function(data){
                 $("#school").html(data);
                });
            });
        }); 
</script>
<script src="assets/vendors/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
@endsection