@extends('be.layouts.index')
@section('title')
list
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Danh sách bài dự thi</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>sbd</th>
                        <th>Fullname</th>
                        <th>DoB</th>
                        <th width=10%;>info</th>
                        {{-- <th>School</th>
                        <th>District</th>
                        <th>Province</th> --}}
                        <th>Parent name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        {{-- <th>Logistic</th> --}}
                        <th>payment</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($contestants as $cont)
                    <tr>
                        <td>{{$cont->id}}</td>
                <td>{{$cont->sbd}}</td>
                <td>{{$cont->fullname}}</td>
                <td>{{$cont->dob}} <br>
                    {{$cont->gender}}</td>
                <td>Class: {{$cont->class}} <br>
                    Grade: {{$cont->grade}} <br>
                    Level: {{$cont->level}}</td>
                {{-- <td><a href="admin/schools/list/{{$cont->school->id}}"> {{$cont->school->name}}</a></td>
                <td>{{$cont->school->districts->name}}</td>
                <td>{{$cont->school->districts->provinces->name}}</td> --}}
                <td>{{$cont->dad_name}} <br>
                    {{$cont->mom_name}} <br>
                    {{$cont->parentname}}
                </td>
                <td>{{$cont->email}}</td>
                <td>{{$cont->phone}}</td>
                <td>{{$cont->address}}</td>
                {{-- <td>{{$cont->logistic}}</td> --}}
                <td>
                    @if ($cont->payment == 1)
                    paid <i class="fa fa-check"></i> <br>
                        @if ($cont->send_mail == 0)
        @else
        mail <i class="fa fa-check"></i>
                        @endif
                    @else
                    chưa thanh toán
                    @endif
                </td>

                <td class="center"><i class="fa fa-pencil fa-fw"></i><br> <a
                        href="admin/contestants/edit/{{$cont->id}}">Edit</a></td>
                <td class="center">
                    <i class="fa fa-trash-o  fa-fw"></i><br>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#deleteModal"
                        data-id="{{ $cont->id }}" data-fullname="{{ $cont->fullname }}"> Delete</a>
                </td>

                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Xóa đăng ký</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p id="modal-message">Bạn chắc chắn muốn xóa thí sinh này?</p>
                                <form method="post" action="{{route('destroy_cont')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" id="id">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Đóng</button>
                                <button type="submit" class="btn btn-success">Xác nhận</button>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
                </tr>
                @endforeach
                </tbody>


            </table>
        </div>
        <hr>

    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>
<script>
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var fullname = button.data('fullname') // Extract info from data-* attributes
        var id = button.data('id')
        var modal = $(this)
        modal.find('#id').val(id);
        modal.find('#modal-message').html('Bạn chắc chắn muốn xóa thí sinh - ' + fullname + id +'?');
      })
</script>
<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            // "ajax": 'admin/contestants/json',
            // "columns": [
            //     { "data": "id" },
            //     { "data": "name" },
            //     { "data": "fullname" },
            //     { "data": "dob" },
            //     { "data": "gender" },
            //     { "data": "class" },
            //     { "data": "grade" },
            //     { "data": "level" },
            //     { "data": "school_id" },
            //     { "data": "level" },
            //     { "data": "level" },
            //     { "data": "parentname" },
            //     { "data": "email" },
            //     { "data": "phone" },
            //     { "data": "address" },
            //     { "data": "logistic" },
            //     { "data": "payment",
            //     render: function(data, type, row){
            //     if(parseInt(data) == 0){
            //         return 'chưa thanh toán';
            //     }
            //     else if(parseInt(data) == 1){
            //         return 'đã thanh toán';
            //     }
            //      }
            //     },
            //     { "data": "phone" },
            //     { "data": "address" },
            // ]
        });
    })
</script>
@endsection