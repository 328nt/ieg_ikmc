<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Testing QR code</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript">
        function generateBarCode()
        {
            var nric = $('#text').val();
            var url = 'https://api.qrserver.com/v1/create-qr-code/?data=' + nric + '&amp;size=50x50';
            $('#barcode').attr('src', url);
        }
    </script>

	<style type="text/css">

		body {
			font-family: 'DejaVu Sans';
		}

		.info-box {
			/* border: 1px solid #333; */
			padding: 20px;
		}

		.text-center {
			text-align: center;
		}

		.title {
			font-size: 18px;
		}

		.flex-item {
			display: table-cell;

		}

		.table {
			display: table;
			width: 100%;
			table-layout: fixed;
			font-size: 14px;
		}

		.table-row {
			display: table-row;

		}

		.table-cell {
			display: table-cell;
			width: 33%;
			overflow-wrap: normal;

		}

		.caption {
			font-size: 14px;
		}

		.font-weight-bold {
			font-weight: bold;
		}

		.c-5 {
			width: 50%;
		}

		.col-3 {
			width: 30%;
		}

		.c-2 {
			width: 20%
		}
	</style>
</head>

<body>
	<div class="main">
		<div class="info-box full-width center">
			<table class="table">
				<tr class="table-row">
                    <td>
                        <form action="{{url('qrex')}}" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <label>text insert</label>
                            <input type="text" class="form-control" name="text">
                            <input type="submit" name="submit" value="Submit" class="submit-btn">
                        </form>
                    </td>
                    @if (isset($text))
					<td >
						{{-- <img width="300px" height="300px"
                    src="https://chart.googleapis.com/chart?cht=qr&chl={{$text}}&chs=160x160&chld=L|0"> --}}
                    <img id='barcode' 
        src="https://api.qrserver.com/v1/create-qr-code/?data={{$text}}&amp;size=100x100" 
        alt="" 
        title="HELLO" 
        width="200" 
        height="200" />
					</td>
                    @endif
				</tr>
			</table>
		</div>
	</div>
</body>

</html>