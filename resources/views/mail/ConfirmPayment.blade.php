<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Congratulation</title>

  <style>
    body {
      font-family: "SFU Futura", Helvetica, Arial, sans-serif;
      word-spacing: 1px;
      text-align: center;
      font-size: 20px;
      color: black;
      line-height: 22pt
    }

    i {
      font-size: 24px;
    }

    p {
      font-size: 16px;
    }

    img {
      width: 100%;
    }

    .flex {
      display: flex;
    }

    .main-color {
      color: #00a6e2;
    }

    .btn-email {
      box-shadow: #00a6e2 0px 1px 0px 0px;
      background: linear-gradient(#00a6e2 5%, #00a6e2 100%) #00a6e2;
      border-radius: 6px;
      border: 1px solid #00a6e2;
      display: inline-block;
      cursor: pointer;
      color: white;
      font-family: "SFU Futura", Helvetica, Arial, sans-serif;
      font-size: 15px;
      text-decoration: none;
      padding: 6px 13px;
      margin-top: -4px;
    }

    .btn-upgrade {
      box-shadow: #00a6e2 0px 1px 0px 0px;
      background: linear-gradient(#00a6e2 5%, #00a6e2 100%) #00a6e2;
      border-radius: 6px;
      border: 1px solid #00a6e2;
      display: inline-block;
      cursor: pointer;
      color: white;
      font-family: "SFU Futura", Helvetica, Arial, sans-serif;
      font-size: 17px;
      font-weight: bold;
      padding: 11px 24px;
      text-decoration: none;
      text-shadow: #00a6e2 0px 1px 0px;
      margin-bottom: 20px;
    }

    .title {
      text-align: left;
      font-family: none;
    }

    .align-center {
      text-align: center;
    }

    .align-left {
      text-align: left;
    }

    .header-font-size {
      font-size: 24px;
    }

    .default-font-size {
      font-size: 20px;
    }

    #img-content {
      padding-left: 4%;
    }
  </style>
</head>

<body>

  <div class="flex" style="width: 100%">
    <div style="width: 10%"></div>
    <div style="width: 80%;" style="text-align: center">
      <div>
        <img
          src="https://lh3.googleusercontent.com/-qYh0c5eWg44/XaPgJHrio8I/AAAAAAAAAr4/aDTka-UTqVoxhQneiWdenzEfAnXa5qCtwCK8BGAsYHg/s0/kmc%2Bsu%25CC%259B%25CC%2589a-07.png"
          alt="">
      </div>

      <div class="flex">
        <div style="width: 5%"></div>

        <div class="align-center" style="width: 90%">
          <div>

            <div>
              <i class="title main-color ">
                CHÀO MỪNG BẠN ĐẾN TRANG WEB <br>
                HỌC TOÁN ONLINE KANGAROO !
              </i>
            </div>
            <div class="content">
              
                <p class="default-font-size">
                    Chúc mừng thí sinh <b>{{$contestants->fullname}}</b> đã thanh toán thành công<br>
                  </p>
              <p class="default-font-size">
                Ban tổ chức cuộc thi IKMC đã nhận được thông tin thanh toán của bạn<br>
                &nbsp; Hiện tại bạn có thể vào học thử<br>
              </p>
            </div>
            <div class=" align-center ">
              <a class=" btn-upgrade" href="http://kangaroo-math.vn/courses-intro#register_i" target="_blank">TẠI
                ĐÂY</a>
            </div>
            <div class=" default-font-size">
              Để cập nhật các thông tin về kỳ thi, vui lòng theo dõi tại <br> fanpage Facebook chính thức của IKMC Việt
              Nam <br>
              <a href="https://www.facebook.com/IKMCVietnam" class="main-color" style="text-decoration: none">Vietnam
                Kangaroo Math Cont</a>
            </div>

            <div class="align-center">

              <p class=" header-font-size">MỌI THẮC MẮC VUI LÒNG LIÊN HỆ</p>

              <h3 class="">097 150 0120 | 098 104 8228 | 093 625 5598</h3>

              <p class="btn-email">kangarooclub@ieg.vn</p>
              <br>
              <i>Xin chân thành cảm ơn !</i>
            </div>

          </div>
        </div>

        <div style="width: 5%"></div>
      </div>

      <div>
        <img
          src="https://lh3.googleusercontent.com/-stCwiLgaBwQ/XaPgShlBCTI/AAAAAAAAAsE/BXdFu7b5LWEdRYcT2ZH2TSTGZRIEcgBggCK8BGAsYHg/s0/kmc%2Bsu%25CC%259B%25CC%2589a-08.png"
          alt="">
      </div>

    </div>
    <div style="width: 10%"></div>
  </div>
</body>

</html>