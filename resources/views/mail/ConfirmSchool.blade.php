<!DOCTYPE html>
<div lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Congratulation</title>

    <style>
      body {
        font-family: "SFU Futura", Helvetica, Arial, sans-serif;
        word-spacing: 1px;
        text-align: center;
        font-size: 20px;
        color: black;
        line-height: 22pt
      }

      i {
        font-size: 24px;
      }

      p {
        font-size: 16px;
      }

      img {
        width: 100%;
      }

      .flex {
        display: flex;
      }

      .main-color {
        color: #00a6e2;
      }

      .btn-email {
        box-shadow: #00a6e2 0px 1px 0px 0px;
        background: linear-gradient(#00a6e2 5%, #00a6e2 100%) #00a6e2;
        border-radius: 6px;
        border: 1px solid #00a6e2;
        display: inline-block;
        cursor: pointer;
        color: white;
        font-family: "SFU Futura", Helvetica, Arial, sans-serif;
        font-size: 15px;
        text-decoration: none;
        padding: 6px 13px;
        margin-top: -4px;
      }

      .btn-upgrade {
        box-shadow: #00a6e2 0px 1px 0px 0px;
        background: linear-gradient(#00a6e2 5%, #00a6e2 100%) #00a6e2;
        border-radius: 6px;
        border: 1px solid #00a6e2;
        display: inline-block;
        cursor: pointer;
        color: white;
        font-family: "SFU Futura", Helvetica, Arial, sans-serif;
        font-size: 17px;
        font-weight: bold;
        padding: 11px 24px;
        text-decoration: none;
        text-shadow: #00a6e2 0px 1px 0px;
        margin-bottom: 20px;
      }

      .title {
        text-align: left;
        font-family: none;
      }

      .align-center {
        text-align: center;
      }

      .align-left {
        text-align: left;
      }

      .header-font-size {
        font-size: 24px;
      }

      .default-font-size {
        font-size: 20px;
      }

      #img-content {
        padding-left: 4%;
      }
    </style>
  </head>

  <body>

    <div class="flex" style="width: 100%">
      <div style="width: 10%"></div>
      <div style="width: 80%;" style="text-align: center">
        <div>
          <img return;
            src="https://lh3.googleusercontent.com/-SkeUT47VaPQ/XaPx1lVLLOI/AAAAAAAAAso/r9EnheYvs7YPJihLqzWZxuaFmLGFN5j8QCK8BGAsYHg/s0/kmc%2Bsu%25CC%259B%25CC%2589a-10.png"
            alt="">
        </div>

        <div class="flex">
          <div style="width: 5%"></div>

          <div class="align-center" style="width: 90%">
            <div>

              <div>
                <i class="title main-color">
                  CHÀO MỪNG BẠN ĐẾN TRANG WEB <br>
                  HỌC TOÁN ONLINE KANGAROO !
                </i>
              </div>

              <div class="content">
                <!-- <p class="default-font-size">
                  Chúc mừng bạn đã đăng ký thành công gói combo IKMC
                  2020 và nâng cấp tài khoản full member! <br>
                </p> -->
                <h2> HƯỚNG DẪN ĐĂNG NHẬP TÀI KHOẢN HỌC TOÁN KANGAROO ONLINE </h2><br>

                <div style="font-size: 18px;">
                  Truy cập website: <a href="https://kangaroo-math.vn/club">https://kangaroo-math.vn/club</a> - <a
                    href="https://www.youtube.com/watch?v=zVyqN3FWF98&t=32s">Video hướng dẫn</a>
                  <br> <br> Tài
                  khoản đẳng nhập clb ikmc: <b>{{$contestants->email}}</b> - Mật khẩu
                  đăng nhập web: <span class=" main-color">12345678</span>

                </div>
                <h3>Chú ý: Nếu đã có tài khoản vui lòng sử dụng mật khẩu cũ</h3>

              </div>

              <!-- <div class="align-center">
                <a class="btn-upgrade"
                  href="https://kangaroo-math.vn/ikmc-register/combo"
                  target="_blank">CÁCH THỨC THANH TOÁN</a>
              </div> -->
              <div class="default-font-size">
                Để cập nhật các thông tin về kỳ thi, vui lòng theo dõi tại <br>
                fanpage Facebook chính thức của IKMC Việt
                Nam <br><br>
                <!-- <span href="https://www.facebook.com/IKMCVietnam">asdad</span> -->
                <a href="https://www.facebook.com/IKMCVietnam" class="main-color" style="text-decoration: none">Vietnam
                  Kangaroo Math Contest</a>
              </div>

              <div class="align-center">

                <p class="header-font-size">MỌI THẮC MẮC VUI LÒNG LIÊN HỆ</p>
                <h3 class="">097 150 0120 | 098 104 8228 | 093 625 5598</h3>

                <p class="btn-email">kangarooclub@ieg.vn</p>
                <br>
                <i>Xin chân thành cảm ơn !</i>
              </div>

            </div>
          </div>

          <div style="width: 5%"></div>
        </div>

        <div>
          <img
            src="https://lh3.googleusercontent.com/-3J_jCZI8ZCI/XaPx6ggot0I/AAAAAAAAAs0/2yZ4ypOR-08YYrPZSlaNK2AGUV3vZt6BACK8BGAsYHg/s0/kmc%2Bsu%25CC%259B%25CC%2589a-22.png"
            alt="">
        </div>

      </div>
      <div style="width: 10%"></div>
    </div>
  </body>

</div>