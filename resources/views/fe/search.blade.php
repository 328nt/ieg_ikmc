@extends('fe.layouts.index')
@section('title')
search
@endsection
@section('content')

@include('msg')
<div class="col-md-8">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">
                <h1>Search</h1>
            </div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>

    </div>
</div>

<div class="col-md-12">


    <div class="ibox-body">
        <form action="{{route('result')}}" method="post" class="form-horizontal" id="form-sample-1">
            {{ csrf_field() }}
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group" id="fullname_container">
                    <input type="text" name="full_name" placeholder="Họ và tên" id="fullname" class="form-control">
                </div>
                <div class="form-group" id="fullname_container">
                    <input type="text" name="dob" placeholder="Ngày sinh" id="birthday" class="form-control">
                </div>
                <button class="btn btn-success" id="search_btn">Search</button>
            </div>
        </form>
    </div>
</div>

@endsection
@section ('script')
<script>

</script>
@endsection