@extends('fe.layouts.index')
@section('title')
list
@endsection
@section('content')

@include('msg')

<div id="result" class="container-fluid ">
    <div class="row">
        @if(isset($cont))
        <div class="col-md-6 col-md-offset-3  col-sm-6 col-xs-12">

            <div class="card">
                <div class="card-header text-center" style="background-color:rgb(244, 164, 66);">
                    <h3>{{$cont->fullname}}</h3>
                    <span class="score-label">SBD: </span><span
                        class="score">{{ str_pad($cont->id,6,'0',STR_PAD_LEFT) }}<span
                            style="font-size: 18px"></span></span>
                    <span class="prize-label">Ngày sinh: </span><span class="prize">{!!
                        $cont->dob !!}</span><br>

                </div>
                <div class=
                <div class="text-center">
                    <h4 class="text-center">Thông tin thí sinh </h4>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="info-label bold-text">Cấp độ: </span><span
                                class="info-text">{{ $cont->level }}</span><br>
                            <span class="info-label bold-text">Lớp: </span><span
                                class="info-text">{{ $cont->class }}</span><br>
                        </div>
                        <div class="col-md-6">

                            <span class="info-label bold-text">Khối: </span><span
                                class="info-text">{{ $cont->grade }}</span><br>
                            <span class="info-label bold-text">Trường: </span><span
                                class="info-text">{{$cont->school->name}}</span>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>


        </div>
        @else
        <div class="col-xs-12 col-md-12 text-center" id="result">
            <h2 class="text-center">KHÔNG TÌM THẤY KẾT QUẢ PHÙ HỢP</h2>
        </div>

        @endif

    </div>
</div>

</div>
<!-- Container (Services Section) -->
@endsection