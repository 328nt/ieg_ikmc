@extends('fe.layouts.index')
@section('title')
Register
@endsection
@section('content')

@include('msg')
<div class="col-md-8">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">
                <h1>Thêm bài dự thi</h1>
            </div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>

    </div>
</div>
<div style="height: 650px;
background-color: red;
background-image: linear-gradient(to right bottom , #fff, #5d1e62);">

    <div class="col-md-10 col-md-offset-1">


        <div class="ibox-body">
            <form action="{{route('register')}}" method="post" class="form-horizontal" id="form-sample-1"
                novalidate="novalidate">
                {{ csrf_field() }}
                <div class="col-md-6">
                    <div>
                        <h3>Thông tin thí sinh</h3>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tên</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="name" type="text" placeholder="Tên">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tên đầy đủ</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="fullname" type="text" placeholder="Tên đầy đủ">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Ngày sinh</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" id="date" name="dob" placeholder="dd/mm/yyyy" />
                            {{-- <input class="form-control" type="text" id="date"   placeholder="dd/mm/yyyy"> --}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Giới tính</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="gender" id="">
                                <option value="0">Nam</option>
                                <option value="1">Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label">Tỉnh/Thành phố</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="province" id="province">
                                <option value="" selected disabled hidden>--Chọn tinh/thành phố--</option>
                                @foreach ($provinces as $prov)
                                <option value="{{ $prov->id }}">{{ $prov->name }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label">Quận/Huyện</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="district" id="district">
                                <option value="" selected disabled hidden>--Chọn quận/huyện--</option>
                                @foreach ($districts as $dist)
                                <option value="{{ $dist->id }}">{{ $dist->provinces->name }} {{ $dist->name }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label">Trường</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="school_id" id="school">
                                <option value="" selected disabled hidden>--Chọn trường--</option>
                                @foreach ($schools as $scl)
                                <option value="{{ $scl->id }}">{{ $scl->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Khối</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="grade" id="">
                                <option value="" selected disabled hidden>--Chọn khối--</option>
                                <option value="1">Khối 1</option>
                                <option value="2">Khối 2</option>
                                <option value="3">Khối 3</option>
                                <option value="4">Khối 4</option>
                                <option value="5">Khối 5</option>
                                <option value="6">Khối 6</option>
                                <option value="7">Khối 7</option>
                                <option value="8">Khối 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Lớp</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="class" type="text" placeholder="class">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Cấp độ</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="level" id="">
                                <option value="" selected disabled hidden>--Chọn cấp độ--</option>
                                <option value="1">Cấp độ 1</option>
                                <option value="2">Cấp độ 2</option>
                                <option value="3">Cấp độ 3</option>
                                <option value="4">Cấp độ 4</option>
                                <option value="5">Cấp độ 5</option>
                                <option value="6">Cấp độ 6</option>
                                <option value="7">Cấp độ 7</option>
                                <option value="8">Cấp độ 8</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <h3>Thông tin phụ huynh</h3>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tên Phụ huynh</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="parentname" type="text" placeholder="parentname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="email" type="text" placeholder="Email address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">sđt</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="phone" type="text" placeholder="mobile">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Địa chỉ</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="address" type="text" placeholder="address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Địa chỉ nhận sách</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="logistic" type="text" placeholder="logistic">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-8 ml-sm-auto">
                            <button class="btn btn-info" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section ('script')
<?php//AJAX CHECK SELECTION?>
<script>
    $(document).ready(function() {
        $("#province").change(function(){
            var prov_id = $(this).val();
            $.get("ajax/district/"+prov_id, function(data){
             $("#district").html(data);
            });
        });
    }); 
    $(document).ready(function() {
        $("#district").change(function(){
            var district_id = $(this).val();
            $.get("ajax/school/"+district_id, function(data){
             $("#school").html(data);
            });
        });
    }); 
</script>
<script>
    var date = document.getElementById('date');

function checkValue(str, max) {
  if (str.charAt(0) !== '0' || str == '00') {
    var num = parseInt(str);
    if (isNaN(num) || num <= 0 || num > max) num = 1;
    str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
  };
  return str;
};

date.addEventListener('input', function(e) {
  this.type = 'text';
  var input = this.value;
  if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
  var values = input.split('/').map(function(v) {
    return v.replace(/\D/g, '')
  });
  if (values[0]) values[0] = checkValue(values[0], 31);
  if (values[1]) values[1] = checkValue(values[1], 12);
  if (values[2]) values[2] = checkValue(values[2], 2000);
  var output = values.map(function(v, i) {
    return v.length == 2 && i < 2 ? v + ' / ' : v;
  });
  this.value = output.join('').substr(0, 14);
});

date.addEventListener('blur', function(e) {
  this.type = 'text';
  var input = this.value;
  var values = input.split('/').map(function(v, i) {
    return v.replace(/\D/g, '')
  });
  var output = '';
  
  if (values.length == 3) {
    var year = values[2].length !== 4 ? parseInt(values[2]) + 2000 : parseInt(values[2]);
    var month = parseInt(values[0]) - 1;
    var day = parseInt(values[1]);
    var d = new Date(year, month, day);
    if (!isNaN(d)) {
      document.getElementById('result').innerText = d.toString();
      var dates = [d.getDate(), d.getMonth() + 1, d.getFullYear()];
      output = dates.map(function(v) {
        v = v.toString();
        return v.length == 1 ? '0' + v : v;
      }).join(' / ');
    };
  };
  this.value = output;
});
</script>
@endsection