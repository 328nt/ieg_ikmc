<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    
    protected $fillable = [
        'id', 'name', 'district_id', 'school_code'
    ];

    public function districts()
    {
        return $this->belongsTo('App\Districts', 'district_id', 'id');
    }
    public function contestants()
    {
        return $this->hasMany('App\Contestants', 'school_id', 'id');
    }
}
