<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\User;
use App\School;

class UserController extends Controller
{
    function __construct()
	{
        $schools = School::all();
        // $districts = Districts::all();
		view::share('schools',$schools);
		// view::share('districts',$districts);
    }
    public function index()
    {
        $users = User::all();
        return view('be/users/list', ['users'=>$users]);
    }

    public function create()
    {
        return view('be/users/add');
    }
    public function store(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        $user = new User();
        $user->name = $rq->name;
        $user->email = $rq->email;
        $user->password = bcrypt($rq->pwd);
        
        $user->save();
        return redirect('admin/users/list')->with('msg','thêm quản trị viên thành công');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('be/users/edit',['user'=>$user]);
    }
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[

        ],[

        ]);
        $user = User::find($id);
        $user->name = $rq->name;
        $user->email = $rq->email;
        if ($rq->changepwd == "on")
        {
            $this->validate($rq,[

            ],[

            ]);
            $user->password = bcrypt($rq->pwd);
        }
        $user->save();
        return redirect()->back()->with('msg','sửa thông tin thành công');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('admin/users/list')->with('msg','xóa quản trị viên thành công !');
    }

    public function getlogin()
    {
        return view('be/login');
    }

    public function postlogin(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        if (Auth::attempt(['email' => $rq->email, 'password' => $rq->password])) {
            return redirect('admin/contestants/sort_list');
        } else {
            
            return redirect('admin/login')->with('msg','sai tài khoản rồi thím ơi !');
        }
        
    }
    public function logoutadmin()
    {
        Auth::logout();
        return redirect('admin/login')->with('msg','Đăng xuất thành công bạn ôi !');
    }
}
