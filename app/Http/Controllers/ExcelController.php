<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\ProvincesExport;
use App\Imports\ProvincesImport;
use App\Imports\DistrictsImport;
use App\Imports\SchoolImport;
use App\Imports\ContestantsImport;
use App\Imports\ContestantSchoolImport;
use App\Exports\ContestantsExport;
use App\Exports\MailConfirmExport;
use App\Imports\SendMailConfirm;
use App\Exports\ComboPaymentExport;
use App\Imports\PaymentImport;
use App\Imports\update_contestants;
use App\Imports\PaymentscodeImport;
use App\Exports\PaymentsExport;
use App\Mail\Confirmpayment;
use App\Contestants;
use App\Paymentscode;
use Mail;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function exview()
    {
    $contestants = Contestants::orderBy('id', 'DESC')->paginate(30);
    return view('be/excel', compact('contestants'));
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export_provinces() 
    {
        return Excel::download(new ProvincesExport, 'Provinces.xlsx');
    }

    
    public function export_confirmmail() 
    {
        return Excel::download(new MailConfirmExport, 'ListMail.xlsx');
    }

    
    public function sendmailconfirm() 
    {
        $conts = Excel::toCollection(new SendMailConfirm,request()->file('file'));
        // dd($conts);  
        $i = 1; 
        foreach ($conts[0] as $row) {
            $contestants = Contestants::where('sbd', $row['sbd'])->first();
            // dd($contestants);
            if($contestants == Null)
            {
                echo $i." : phát hiện lỗi: SBD: .''.$row[sbd] ";
                $i++;
                continue;
            }
            $contestants->send_mail = 1;
            $contestants->update();
            
        Mail::to($row['email'])->send(new Confirmpayment($contestants));
        }
        // return back()->with('msg','okla'); 
    }



    /**
    * @return \Illuminate\Support\Collection
    */
    public function import_provinces() 
    {
        Excel::import(new ProvincesImport,request()->file('file'));

        return back();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import_districts() 
    {
        Excel::import(new DistrictsImport,request()->file('file'));

        return back()->with('msg','okla');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import_school() 
    {
        Excel::import(new SchoolImport,request()->file('file'));
        return back()->with('msg','okla');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    // public function import_payment2() 
    // {
    //     Excel::import(new PaymentImport,request()->file('file'));
    //     return back()->with('msg','okla');
    // }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import_contestants() 
    {
        $contestants = Excel::toCollection(new ContestantsImport,request()->file('file'));
        $i = 0;
        // dd($contestants[0]);
        foreach ($contestants[0] as $row) 
        {
        $cont = Contestants::where('sbd', $row['tranfer_code'])->updateOrCreate(
            [
                'sbd' => $row['tranfer_code']
            ],
            [
                    'sbd' => $row['tranfer_code'],
                    'name' => $row['name'],
                    'fullname' => $row['full_name'],
                    'gender' => $row['gender'],
                    'email' => $row['email'],
                    'dob' => $row['dob'],
                    'grade' => $row['grade'],
                    'class' => $row['class'],
                    'level' => $row['level'],
                    'school_id' => $row['school_id'],
                    // 'parentname' => $row['parentname'],
                    // 'phone_teacher' => $row['phone_teacher'],
                    // 'email_teacher' => $row['email_teacher'],
                    'dad_name' => $row['dad_name'],
                    'dad_email' => $row['dad_mail'],
                    'dad_phone' => $row['dad_phone'],
                    'mom_name' => $row['mom_name'],
                    'mom_email' => $row['mom_email'],
                    'mom_phone' => $row['mom_phone'],
                    'address' => $row['address'],
                    // 'logistic' => $row['logistic'],
                    // 'book' => $row['book'],
                    // 'shirt_size' => $row['shirt_size'],
                    'combo' => $row['register_type'],
                    'day_reg' => $row['register_date'],
                    'amount' => $row['price'],
        ]);
        }
        return back()->with('msg','okla'); 
    }

    
    public function contestants_school() 
    {
        ini_set('memory_limit', '512M');
        $contestants = Excel::toCollection(new ContestantSchoolImport,request()->file('file'));
        $i = 0;
            try {
                // dd($contestants[0]);
                foreach ($contestants[0] as $row) 
                // dd(substr($row['dien_thoai_bo'], -3));
                {
                $cont = Contestants::where('sbd', 'S'.''.str_pad($row['ma_truong'], 5, '0', STR_PAD_LEFT).''.substr($row['email_bo'], 0, 3).''.substr($row['dien_thoai_bo'], -3).''.substr($row['email_me'], 0, 3).''.substr($row['dien_thoai_me'], -3).''.str_random(6))->updateOrCreate(
                    [
                        
                        'sbd' => 'S'.''.str_pad($row['ma_truong'], 5, '0', STR_PAD_LEFT).''.substr($row['email_bo'], 0, 3).''.substr($row['dien_thoai_bo'], -3).''.substr($row['email_me'], 0, 3).''.substr($row['dien_thoai_me'], -3).''.str_random(6),
                    ],
                    [
                            'sbd' =>  'S'.''.str_pad($row['ma_truong'], 5, '0', STR_PAD_LEFT).''.substr($row['email_bo'], 0, 3).''.substr($row['dien_thoai_bo'], -3).''.substr($row['email_me'], 0, 3).''.substr($row['dien_thoai_me'], -3).''.str_random(6),
                            'name' => $row['ten'],
                            'fullname' => str_replace('  ', ' ',$row['ho_va_ten_dem'].' '.$row['ten']),
                            'gender' => $row['gioi_tinh'],
                            'email' => $row['email_gvcn'],
                            'dob' => $row['ngay_sinh'].'/'.$row['thang_sinh'].'/'.$row['nam_sinh'],
                            'grade' => $row['khoi_lop'],
                            'class' => $row['lop'],
                            'level' => $row['cap_do'],
                            'school_id' => $row['ma_truong'],
                            'combo' => 'school',
                            'amount' => $row['dang_ky_combo_kmc_online_1_nam'],
                            'phone_teacher' => $row['dien_thoai_gvcn'],
                            'email_teacher' => $row['email_gvcn'],
                            'dad_name' => $row['ho_va_ten_bo'],
                            'dad_phone' => $row['dien_thoai_bo'],
                            'dad_email' => $row['email_bo'],
                            'mom_name' => $row['ho_va_ten_me'],
                            'mom_phone' => $row['dien_thoai_me'],
                            'mom_email' => $row['email_me'],
                ]);
            }
                return back()->with('msg','okla'); 
            } catch (\Exception $ex) {
                return back()->withErrors($ex->getMessage());
            }
    }
    
    public function update_contestants_school() 
    {
        $contestants = Excel::toCollection(new ContestantSchoolImport,request()->file('file'));
        
        $i = 1; 
        foreach ($contestants[0] as $row) {
            $contestants = Contestants::where('sbd',  'SCHOOL'.''.str_pad($row['ma_truong'], 5, '0', STR_PAD_LEFT).''.substr($row['email_bo'], 0, 3).''.substr($row['email_me'], 0, 3))->first();
            // dd($contestants);
            if($contestants == Null)
            {
                echo $i." : phát hiện lỗi: SBD: <br>";
                $i++;
                continue;
            // break;
            }
            $contestants->payment = 1;
            // $contestants->combo = 'school';
            $contestants->send_mail = 2;
            $contestants->update();

        }
        return back()->with('msg','okla'); 
    }

    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function update_contestants() 
    {
        $conts = Excel::toCollection(new update_contestants,request()->file('file'));
        // dd($conts);  
        $i = 1; 
        foreach ($conts[0] as $row) {
            $contestants = Contestants::where('sbd', substr(strstr( strtoupper($row['content']), 'IKMC0'), 0, 9))->where('amount', $row['total'])->first();
            // dd($contestants);
            if($contestants == Null)
            {
                echo $i." : phát hiện lỗi: SBD: <b>".substr(strstr( strtoupper($row['content']), 'IKMC0'), 0, 9)."</b> - Với số tiền <b>".$row['total']."</b> - mã chuyển khoản <b>".$row['reference']."</b> - số <b>".$row['number']."</b><br>";
                $i++;
                continue;
            }
            $contestants->payment = 1;
            $contestants->reference = $row['reference'];
            $contestants->number = $row['number'];
            // $contestants->send_mail = 1;
            $contestants->update();
            // Mail::to($contestants->email)->send(new Confirmpayment($contestants));
           }
        // return back()->with('msg','okla'); 
    }

        
    // public function update_contestants() 
    // {
    //     $conts = Excel::toCollection(new update_contestants,request()->file('file'));
    //     $arrayName = array('json' => $conts, );
    //     dd($arrayName);
    //     $cont_json = json_encode(Array($conts));
    //     $cont_json = json_decode($cont_json, true);
    //     $jsons = $cont_json[0];
    //     dd($jsons);
    //     $i = 0; 
    //     foreach ($cont_json[0] as $json) {
    //         dd($json['status']);
    //         $contestants = Contestants::where('id', $json['id'])->first();
    //         dd($contestants);
    //         if ($contestants == null) {
    //             echo $i." : Không tìm thấy SBD: ".$json['id']."<br>";
    //             $i++;
    //             continue;
    //         }
    //         $contestants->payment = $json['status'];
    //         $contestants->update();
    //     return back()->with('msg','okla'); 
    //     }
    // }



    public function export_contestants() 
    {
        return Excel::download(new ContestantsExport, 'Contestants_local.xlsx');
        
    }

    public function export_combo() 
    {
        return Excel::download(new ComboPaymentExport, 'School_Combo.xlsx');
        
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function payment() 
    // {
    //     Excel::import(new PaymentImport,request()->file('file'));
    //     return back()->with('msg','okla'); 
    // }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import_payments() 
    {
        $payments = Excel::toCollection(new PaymentscodeImport,request()->file('file'));
        $i = 0; 
        foreach ($payments[0] as $row) {
            
        $cont = Paymentscode::where('number', $row['number'])->updateOrInsert(
            [
                'number' => $row['number']
            ],
            [
            'payments_text' => $row['content'],
            'reference' => $row['reference'],
            'payments_id' => substr(strstr( strtoupper($row['content']), 'IKMC0'), 0, 9),
            'amount' => $row['total'],
            'number' => $row['number'],
        ]);
        
            
        echo $i." : đã update: SBD: <b>".$row['number']."</b> <br>";
        $i++;
        continue;
            }
        // return back()->with('msg','okla'); 
    }

    public function export_payments() 
    {
        return Excel::download(new PaymentsExport, 'Payment.xlsx');
    }
}
