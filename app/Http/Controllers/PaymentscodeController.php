<?php

namespace App\Http\Controllers;

use App\Paymentscode;
use Illuminate\Http\Request;

class PaymentscodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paymentscode  $paymentscode
     * @return \Illuminate\Http\Response
     */
    public function show(Paymentscode $paymentscode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paymentscode  $paymentscode
     * @return \Illuminate\Http\Response
     */
    public function edit(Paymentscode $paymentscode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paymentscode  $paymentscode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paymentscode $paymentscode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paymentscode  $paymentscode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paymentscode $paymentscode)
    {
        //
    }
}
