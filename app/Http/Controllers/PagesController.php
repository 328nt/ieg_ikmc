<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Datatables;
use App\Mail\RegisterEmail;
use App\Mail\Confirmpayment;
use App\Mail\ConfirmSchool;
use App\School;
use App\User;
use App\Districts;
use App\Province;
use App\Contestants;
use Mail;

class PagesController extends Controller
{   
    // public function index(){
    //     $users= DB::table('contestants')->get();
    //     return response()->json($users, 200);
    // }

    // public function show($id){
    //     if($user = DB::table('contestants')->find($id)){
    //         return response()->json($user, 200);
    //     }
    //     else{
    //         return response()->json("user not found", 200);
    //     }
    // }
    // public function store(Request $request){
    //     if(DB::table('contestants')->insert($request->all())){
    //         return response()->json("User has been created",201);
    //     }
    //     else{
    //         return response()->json("Can not create");
    //     }   
    // }


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            $users = User::select(['id', 'name', 'email', 'created_at', 'updated_at'])->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
                    
        DataTables::of($users)
        ->editColumn('avatar', function ($user) {
            return '<img src="' . $user->avatar . '" alt="" class="img-circle img-avatar-list">';
        })
        ->addColumn('fff', function ($user) {
            return '<a href="'. route('admin.users.show', $user->id) .'" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> View</a> <a href="javascript:void(0)" data-id="' . $user->id . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
        })
        ->rawColumns(['avatar', 'fff'])
        ->make(true);
        }
        return view('be.school.index');
    }

    // public function getIndex()
    // {
    //     return view('schools.index');
    // }

    // /**
    //  * Process datatables ajax request.
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function anyData()
    // {
    //     return Datatables::of(School::query())->make(true);
    // }



    public function getdistrict($prov_id)
    {
        $district = Districts::where('prov_id', $prov_id)->get();

        foreach ($district as $dist) {
            echo "<option value ='".$dist->id."'>".$dist->id." ".$dist->name."</option>";
        }
    }

    public function getschool($district_id)
    {
        $school = School::where('district_id', $district_id)->get();

        foreach ($school as $scl) {
            echo "<option value ='".$scl->id."'> ".$scl->name."</option>";
        }
    }
    public function schooltodistrict($id)
    {
        $school = School::find($id);
        $sc = $school->name;
        echo $sc;
    }


    public function register()
    {
        $schools = School::all();
        $districts = Districts::all();
        $provinces = Province::all();
        return view('fe/register', ['schools'=>$schools, 'districts'=>$districts, 'provinces'=>$provinces]);
    }
    public function postregister(Request $rq)
    {
        $this->validate($rq, [

        ],[

        ]);
        $contestants =new Contestants();
        $contestants->name = $rq->name;
        $contestants->fullname = $rq->fullname;
        $contestants->email = $rq->email;
        $contestants->gender = $rq->gender;
        $contestants->dob = $rq->dob;
        $contestants->grade = $rq->grade;
        $contestants->class = $rq->class;
        $contestants->level = $rq->level;
        $contestants->school_id = $rq->school_id;
        $contestants->parentname = $rq->parentname;
        $contestants->phone = $rq->phone;
        $contestants->address = $rq->address;
        $contestants->logistic = $rq->logistic;
        $contestants->save();
            // dd(date("jS-F-Y", strtotime($contestants->created_at)));
        Mail::to($rq->email)->send(new RegisterEmail($contestants));
        return redirect()->back()->with('msg', 'oola ole bleubleu');

    }


    public function search()
    {
        return view('fe.search');
    }

    public function search_result(Request $rq)
    {
        $fullname = trim($rq->full_name);
        $fullname = str_replace('  ', ' ', $fullname);
        $dob = $rq->dob;
        // dd($dob);
        // $date = str_replace('/', '-', $dob);
        // $dob = date('Y-m-d', strtotime($date));
        $cont = Contestants::where('fullname', $fullname)->where('dob', $dob)->first();
        // dd($cont);
        $data['cont'] = $cont;
        return view('fe.search_result', $data);

    }

    public function formtest()
    {
        return view('mail.testmail');
    }
    public function testmail(request $req)
    {
        $id = $req->id;
        $contestants = Contestants::find($id);
        $contestants->send_mail = 1;
        $contestants->save();
        Mail::to($req->email)->send(new Confirmpayment($contestants));

    // $mailid = $req->email;
    // $name = $req->name;
    // $subject = 'Xác nhận thanh toán IKMC 2020';
    // $data = array('email' => $mailid, 'name' =>$name ,'subject' => $subject);
    // Mail::send('mail.ConfirmPayment', $data, function ($message) use ($data) {
    // $message->from('kangarooclub@ieg.vn', 'IKMC2020');
    // $message->to($data['email']);
    // $message->subject($data['subject']); 
    // });
    return redirect()->back()->with('msg','Successfully Send Mail');
    }

    
    public function school_mail(request $req)

    
    {
        // $id = $req->id;
        // $contestants = Contestants::find($id);
        // $contestants->save();
        $contestant = $req->email;
        
        // $contestant = ["nhanntt@hvhv.edu.vn", "tuongminhhien@gmail.com", "lien.nth@theolympiaschools.edu.vn", "maithimen.cet@gmail.com", "hongphaninfo@gmail.com", "maianh200881@gmail.com", "dangchungvibex@gmail.com", "lamgiang.bravat@gmail.com", "tieulong155@gmail.com", "trinh@clevergroup.vn"];
        
        $contestant = json_decode($contestant);
        // dd($contestant);
        foreach($contestant as $contestants){

            // dd($contestants->Email);
            Mail::to($contestants->email)->send(new ConfirmSchool($contestants));
        }

    return redirect()->back()->with('msg','ô kê rồi đấy !!!!');
    }

    
    public function fixmybad(request $req)
    {
        $id = $req->id;
        $contestants = Contestants::find($id);
        // dd($id);
        $contestants->send_mail = 0;
        $contestants->save();
        Mail::to($req->email)->send(new RegisterEmail($contestants));
    return redirect()->back()->with('msg','Successfully Send Mail');
    }

    
    public function qr()
    {
        return view('qr');
    }
    public function qrex(request $req)
    {
        $text = $req->text;
        $data['text'] = $text;
        // dd($data);
    return view('qr', compact('text'));
    }
}
