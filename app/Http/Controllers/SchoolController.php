<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;

use App\School;
use App\Districts;
use App\Province;
use DB;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    function __construct()
	{
    }
    
    
    public function json()
    {
        $schools = School::all();
        $schools_array = $schools->toArray();
        $data['data'] = $schools_array;
        // dd($data['data']);
        $json = json_encode($data);
        // dd($json);
        echo $json;
    }

    public function json_dis()
    {
        $districts = Districts::all();
        $districts_array = $districts->toArray();
        $data['data'] = $districts_array;
        // dd($data['data']);
        $json = json_encode($data);
        // dd($json);
        echo $json;
    }

    public function all()
    {
        $schools = School::all();
        $chunks = $schools->chunk(1000);
        // dd($chunks);
        return view('be.school.all', compact('chunks', 'schools'));
    }
    public function schoolreg()
    {
        return view('be.school.schoolreg');
    }
    public function schoolpaid()
    {
        $school = School::where('payment', 1)->get();
        // dd($school);
        return view('be.school.schoolpaid', compact('school'));
    }
    public function schoolunpaid()
    {
        $school = School::where('payment', 0)->get();
        return view('be.school.schoolunpaid', compact('school'));
    }

    public function schooledit($id)
    {
        $school = School::find($id);
        return view('be.school.schooledit', compact('school'));
    }
    public function schoolupdate(Request $rq, $id)
    {
        $this->validate($rq, [

        ],[

        ]);

        $school = School::find($id);
        $school->payment = $rq->payment;
        $school->save();
        return redirect()->back()->with('msg', 'update school success !!!');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $school = School::find($id);
        return view('be.school.list',['school'=>$school]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        $districts = Districts::all();
        return view('be/school/add', ['provinces'=>$provinces, 'districts'=>$districts]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        $user = new School();
        $user->id = $rq->id;
        $user->district_id = $rq->district;
        $user->school_code = $rq->school;
        $user->name = $rq->school;
        
        $user->save();
        return redirect('admin/schools/add')->with('msg','thêm trường thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        //
    }
}
