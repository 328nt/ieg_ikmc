<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;

use App\Contestants;
use App\School;
use App\Districts;
use App\Province;
use DB;
use Illuminate\Http\Request;

class ContestantsController extends Controller
{
    function __construct()
	{
        // $schools = School::all();
        // $districts = Districts::all();
        // $provinces = Province::all();
		// view::share('schools',$schools);
		// view::share('districts',$districts);
		// view::share('provinces',$provinces);
    }
    
    public function json()
    {
        $contestants = Contestants::all();
        $contestants_array = $contestants->toArray();
        $data['data'] = $contestants_array;
        // dd($data['data']);
        $json = json_encode($data);
        // dd($json);
        echo $json;
    }

    public function all()
    {
        $schools = School::all();
        $districts = Districts::all();


        return view('be.layouts.index', ['schools'=>$schools, 'districts'=>$districts]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $contestants = Contestants::All();

        // $results = Contestants::whereIn('name', function ( $query ) {
        //     $query->select('id')->from('contestants')->groupBy('id')->havingRaw('count(*) > 1');
        // })->get();
    // dd($results);
        return view('be/contestants/list', ['contestants'=>$contestants]);
    }

    public function sort_list()
    {
        
        $contestants = Contestants::All();

        // $results = Contestants::whereIn('name', function ( $query ) {
        //     $query->select('id')->from('contestants')->groupBy('id')->havingRaw('count(*) > 1');
        // })->get();
    // dd($results);
        return view('be/contestants/sort_list', ['contestants'=>$contestants]);
    }
    public function send_mail()
    {
        
        $contestants = Contestants::All();

        // $results = Contestants::whereIn('name', function ( $query ) {
        //     $query->select('id')->from('contestants')->groupBy('id')->havingRaw('count(*) > 1');
        // })->get();
    // dd($results);
        return view('be/contestants/send_mail', ['contestants'=>$contestants]);
    }

    public function deletecont(Request $request)
    {
        $contestant = Contestants::findOrFail($request->id);
        
            $contestant->delete();
        return redirect()->back()->with('msg','delete success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contestants  $contestants
     * @return \Illuminate\Http\Response
     */
    public function show(Contestants $contestants)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contestants  $contestants
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cont = Contestants::find($id);
        return view('be.contestants.edit', compact('cont'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contestants  $contestants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[

        ],[

        ]);
        $cont = Contestants::find($id);
        $cont->name = $rq->name;
        $cont->fullname = $rq->fullname;
        $cont->dob = $rq->dob;
        $cont->gender = $rq->gender;
        $cont->class = $rq->class;
        $cont->level = $rq->level;
        // $cont->school_id = $rq->school_id;
        $cont->parentname = $rq->parentname;
        $cont->phone = $rq->phone;
        $cont->email = $rq->email;
        $cont->address = $rq->address;
        // $cont->logistic = $rq->logistic;
        $cont->payment = $rq->payment;
        $cont->save();
        return redirect()->back()->with('msg','okla aklo');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contestants  $contestants
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cont = Contestants::find($id);
        $cont -> delete();
        return redirect()->back()->with('msg','delete success');
    }
}
