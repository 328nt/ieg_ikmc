<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmSchool extends Mailable


{
    use Queueable, SerializesModels;
    public $contestants;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contestants)
    {
        $this->contestants = $contestants;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.ConfirmSchool')->subject('Xác nhận Thanh toán IKMC thành công ( theo trường ) !!');
    }
}
