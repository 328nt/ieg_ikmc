<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $contestants;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contestants)
    {
        $this->contestants = $contestants;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('fe.mail.mail_register')->subject('Xác nhận hoàn thành thủ tục đăng ký dự thi');
    }
    
    // public function build()
    // {
    //     return $this->view('mail.fixmybad')->subject('Lỗi hệ thống !!!');
    // }
}
