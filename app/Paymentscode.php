<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paymentscode extends Model
{
    protected $fillable = [
        'payments_text', 'reference', 'payments_id', 'amount', 'number', 'day'
    ];
}
