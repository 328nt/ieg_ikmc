<?php

namespace App\Exports;

use App\Paymentscode;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class PaymentsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('be.payments',[
            'payments'=>Paymentscode::All()
        ]);
    }
}
