<?php

namespace App\Imports;

use App\Districts;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DistrictsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Districts([
            'name' => $row['name'],
            'fullname' => $row['fullname'],
            'email' => $row['email'],
            'gender' => $row['gender'],
            'dob' => $row['dob'],
            'grade' => $row['grade'],
            'class' => $row['class'],
            'level' => $row['level'],
            'school_id' => $row['school_id'],
            'parentname' => $row['parentname'],
            'phone' => $row['phone'],
            'address' => $row['address'],
            'logistic' => $row['logistic'],
            //
        ]);
    }
}
