<?php

namespace App\Imports;

use App\Paymentscode;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PaymentscodeImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Paymentscode([
            
            'payments_text' => $row['content'],
            'reference' => $row['reference'],
            'payments_id' => substr(strstr( strtoupper($row['content']), 'IKMC0'), 0, 9),
            'amount' => $row['total'],
            'number' => $row['number'],
        ]);
    }
    public function headingRow(): int
    {
        return 2;
    }
}
