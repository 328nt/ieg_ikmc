<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContestantSchoolImport implements ToModel, WithHeadingRow
{
    
    public function model(array $row)
    {
        return new Contestants([
            
        ]);
    }
    public function headingRow(): int
    {
        return 12;
    }
}
