<?php

namespace App\Imports;

use App\Contestants;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class update_contestants implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Contestants([
            // 'fullname' => $row[2],
            // 'email' => $row[3],
        ]);
    }
    
    public function headingRow(): int
    {
        return 2;
    }
}
