<?php

namespace App\Imports;

use App\Payments;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PaymentImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Payments([
            
            'reference' => $row['payment'],
            'amount' => $row['amount'],
            'contestant_id' => substr(strstr( $row['payment'], 'ikmc'), 4, 5),
        ]);
    }
}
