<?php

namespace App\Imports;

use App\Contestants;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContestantsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */




    
    public function model(array $row)
    {
        return new Contestants([
            
            'sbd' => $row['tranfer_code'],
            'name' => $row['name'],
            'fullname' => $row['full_name'],
            'gender' => $row['gender'],
            'email' => $row['email'],
            'dob' => $row['dob'],
            'grade' => $row['grade'],
            'class' => $row['class'],
            'level' => $row['level'],
            'school_id' => $row['school_id'],
            // 'parentname' => $row['parentname'],
            // 'phone_teacher' => $row['phone_teacher'],
            // 'email_teacher' => $row['email_teacher'],
            'dad_name' => $row['dad_name'],
            'dad_email' => $row['dad_mail'],
            'dad_phone' => $row['dad_phone'],
            'mom_name' => $row['mom_name'],
            'mom_email' => $row['mom_email'],
            'mom_phone' => $row['mom_phone'],
            // 'address' => $row['address'],
            // 'logistic' => $row['logistic'],
            // 'book' => $row['book'],
            // 'shirt_size' => $row['shirt_size'],
            'combo' => $row['register_type'],
            'day_reg' => $row['register_date'],
            'amount' => $row['price'],
            
        ]);
    }
    // public function headingRow(): int
    // {
    //     return 4;
    // }
}
