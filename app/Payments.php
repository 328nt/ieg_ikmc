<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = ['contestant_id', 'reference'];
    public function contestant()
    {
        return $this->belongsTo('App\Contestants', 'contestant_id', 'id');
    }
}
