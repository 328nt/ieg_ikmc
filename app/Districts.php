<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    
    protected $fillable = [
        'name', 'prov_id',
    ];

    public function provinces()
    {
        return $this->belongsTo('App\Province', 'prov_id', 'id');
    }
    public function school()
    {
        return $this->hasMany('App\School', 'school_id', 'id');
    }

    public function contestants()
    {
        return $this->hasManyThrough('App\Contestant', 'App\School', 'school_id', 'district_id', 'id');
    }
}
