<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//API

Route::get('/api','APIController@curl');
Route::post('/test1', 'APIController@test1');
Route::get('/test2', 'APIController@test2');


Route::get('welcome/{username}', function(Request $request, $username){
    return response()->json("Hello $username!", 200);
});
Route::get('/users','PagesController@index');
Route::get('/users/{id}','PagesController@show');
Route::post('/users','PagesController@store');

Route::get('datatables','PagesController@datatables')->name=('school.index');
Route::get('users', ['uses'=>'PagesController@index', 'as'=>'users.index']);


Route::get('export_provinces', 'ExcelController@export_provinces')->name('export_provinces');
Route::get('export_confirmmail', 'ExcelController@export_confirmmail')->name('export_confirmmail');
Route::post('sendmailconfirm', 'ExcelController@sendmailconfirm')->name('sendmailconfirm');
Route::get('ex', 'ExcelController@exview');
Route::post('import_provinces', 'ExcelController@import_provinces')->name('import_provinces');
Route::post('import_districts', 'ExcelController@import_districts')->name('import_districts');
Route::post('import_school', 'ExcelController@import_school')->name('import_school');
Route::post('import_contestants', 'ExcelController@import_contestants')->name('import_contestants');
Route::post('contestants_school', 'ExcelController@contestants_school')->name('contestants_school');
Route::post('update_contestants_school', 'ExcelController@update_contestants_school')->name('update_contestants_school');
Route::get('export_contestants', 'ExcelController@export_contestants')->name('export_contestants');
Route::get('export_combo', 'ExcelController@export_combo')->name('export_combo');
Route::post('update_contestants', 'ExcelController@update_contestants')->name('update_contestants');

Route::get('sendmailschool', 'PagesController@formtest');
Route::post('testmail','PagesController@testmail');
Route::post('school_mail','PagesController@school_mail');
Route::post('fixmybad','PagesController@fixmybad');


Route::get('qr', 'PagesController@qr');
Route::post('qrex','PagesController@qrex')->name('qrex');

Route::get('jsonprov', 'ProvinceController@ImportJsonFile');
Route::get('jsondist', 'DistrictsController@ImportJsonFile');
Route::get('jsonupdist', 'DistrictsController@UpdateJsonFile');
Route::get('jsonscho', 'SchoolController@ImportJsonFile');

Route::post('import_payments', 'ExcelController@import_payments')->name('import_payments');
Route::get('export_payments', 'ExcelController@export_payments')->name('export_payments');

Route::post('payment', 'ExcelController@payment')->name('payment');

Route::get('admin', 'UserController@getlogin');
Route::get('admin/login', 'UserController@getlogin');
Route::post('admin/login', 'UserController@postlogin')->name('admin_login');
Route::get('admin/logout', 'UserController@logoutadmin');
Route::get('register', 'PagesController@register');
Route::post('register', 'PagesController@postregister')->name('register');


Route::get('/search', 'PagesController@search');
Route::post('/search/result', 'PagesController@search_result')->name('result');

// Route::get('duplicates', function () {
//     $results = \App\Contestants::whereIn('name', function ( $query ) {
//         $query->select('name')->from('contestants')->groupBy('name')->havingRaw('count(*) > 1');
//     })->get();

//     return $results;
// });


Route::group(['prefix' => 'ajax'], function () {
    Route::get('district/{prov_id}', 'PagesController@getdistrict');
    Route::get('school/{district_id}', 'PagesController@getschool');
    Route::get('schooltodistrict/{id}', 'PagesController@schooltodistrict');
    // Route::get('unit/{idlesson}', 'PagesController@getunit');
});



Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    
    Route::get('all', 'ContestantsController@all');
    Route::get('dashboard', function () {
        return view('be/dashboard');
    });

    Route::group(['prefix' => 'users'], function () {
        
        Route::get('list', 'UserController@index');
        Route::get('add', 'UserController@create');
        Route::post('add', 'UserController@store')->name('user_add');
        Route::get('edit/{id}', 'UserController@edit');
        Route::post('edit/{id}', 'UserController@update')->name('user_edit');
        Route::get('delete/{id}', 'UserController@destroy')->name('user_destroy');
    });

    Route::group(['prefix' => 'contestants'], function () {
        Route::get('json', 'ContestantsController@json');
        Route::get('sort_list', 'ContestantsController@sort_list');
        Route::get('send_mail', 'ContestantsController@send_mail');
        Route::get('list', 'ContestantsController@index');
        Route::get('add', 'ContestantsController@create');
        Route::post('add', 'ContestantsController@store')->name('contestants_add');
        Route::get('edit/{id}', 'ContestantsController@edit');
        Route::post('edit/{id}', 'ContestantsController@update')->name('contestants_edit');
        Route::get('delete/{id}', 'ContestantsController@destroy');
        Route::post('delete/{id}', 'ContestantsController@destroy')->name('contestants_destroy');
        Route::post('destroy','ContestantsController@deletecont')->name('destroy_cont');
    });
    
    Route::group(['prefix' => 'schools'], function () {
        
        Route::get('json', 'SchoolController@json');
        Route::get('json_dis', 'SchoolController@json_dis');
        // Route::post('schoolpaid', 'SchoolController@schoolpaid')->name('school_payment');
        Route::get('schoolpaid', 'SchoolController@schoolpaid');
        Route::get('schoolunpaid', 'SchoolController@schoolunpaid');
        Route::get('schooledit/{id}', 'SchoolController@schooledit');
        Route::post('schooledit/{id}', 'SchoolController@schoolupdate')->name('school_edit');
        Route::get('schoolreg', 'SchoolController@schoolreg');
        Route::get('all', 'SchoolController@all');
        Route::get('list/{id}', 'SchoolController@index');
        Route::get('add', 'SchoolController@create');
        Route::post('add', 'SchoolController@store')->name('school_add');
        Route::get('edit/{id}', 'SchoolController@edit');
        Route::post('edit/{id}', 'SchoolController@update')->name('user_edit');
        Route::get('delete/{id}', 'SchoolController@destroy')->name('user_destroy');
    });
});

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
